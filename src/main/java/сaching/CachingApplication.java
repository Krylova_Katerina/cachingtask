package сaching;

import сaching.emulator.CachingEmulator;
import сaching.data.TypeCache;
import сaching.exception.ProhibitedActionException;
import сaching.file.FileWork;
import сaching.file.TextFileWorkImpl;

public class CachingApplication {

    public static void main(final String[] args) {

        System.out.println("Caching");
        if (args.length != 6) {
            throw new ProhibitedActionException("not enough params");
        }

        FileWork fileWork = new TextFileWorkImpl(args[0]);
        CachingEmulator cachingEmulator = new CachingEmulator(fileWork, Long.parseLong(args[1]),
                Long.parseLong(args[2]), args[3], args[4], TypeCache.valueOf(args[5]));
        cachingEmulator.emulateCaching();
    }

}
