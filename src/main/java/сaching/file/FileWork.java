package сaching.file;

import сaching.data.Cache;

import java.util.List;
public interface FileWork {

    List<String> readStringsFromFile();

    void serializationObj(Cache cache, String cachePath);

    void deleteFile(Cache cache, String cachePath);
}
