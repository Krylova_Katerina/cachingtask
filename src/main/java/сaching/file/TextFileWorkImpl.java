package сaching.file;

import сaching.data.Cache;
import сaching.exception.ProhibitedActionException;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class TextFileWorkImpl implements FileWork {
    private final String pathFile;

    public TextFileWorkImpl(final String pathFile) {
        this.pathFile = pathFile;
    }

    @Override
    public List<String> readStringsFromFile() {
        List<String> strings;
        try (BufferedReader br = new BufferedReader(new FileReader(pathFile))) {
            strings = br.lines().filter(readingString -> !readingString.isBlank()).collect(Collectors.toList());
        } catch (IOException ex) {
            throw new ProhibitedActionException("Error read from file", ex);
        }
        return strings;
    }

    @Override
    public void serializationObj(Cache cache, String cachePath) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(cachePath))) {
            out.writeObject(cache);
        } catch (IOException ex) {
            throw new ProhibitedActionException("Error write cache to file", ex);
        }
    }

    @Override
    public void deleteFile(Cache cache, String cachePath){
        new File(cachePath).delete();
    }
}





