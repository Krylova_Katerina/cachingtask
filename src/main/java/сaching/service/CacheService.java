package сaching.service;


import сaching.data.Cache;

public interface CacheService<V> {

    boolean put(Long key, V value);

    boolean validateValueSizeAndPutIfCan(Long key, V value);

    void deleteExtraValue();

    Cache getCacheWithLowerPriority();
}
