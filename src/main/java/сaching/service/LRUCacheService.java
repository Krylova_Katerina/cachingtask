package сaching.service;
import lombok.Getter;
import lombok.Setter;
import сaching.data.LRUCache;
import сaching.exception.ProhibitedActionException;

import java.lang.instrument.Instrumentation;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.TreeSet;

@Getter
@Setter
public class LRUCacheService<V> implements CacheService<V> {
    private TreeSet<LRUCache> caches = new TreeSet<>(Comparator.comparing(LRUCache::getPriority));
    private Long capacity;

    private static Instrumentation instrumentation;

    public LRUCacheService(Long capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean put(Long key, V value) {
        if (!validateValue(value) && !validateSize(value)) { ;
            return false;
        }
        putAndFormNewIfNeed(key, value);
        return true;
    }

    protected void putAndFormNewIfNeed(Long key, V value) {
        if (!validateValue(value) && validateKey(key)) {
            caches.add(new LRUCache(key, value, LocalDateTime.now()));
        } else {
            LRUCache cache = findByKey(key);
            cache.setPriority(LocalDateTime.now());
        }
    }

    private boolean validateKey(Long key){
        if(caches.stream().anyMatch(cache -> cache.getKey().equals(key))){
            throw new ProhibitedActionException("key "+key+" already exist");
        }
        return true;
    }

    @Override
    public boolean validateValueSizeAndPutIfCan(Long key, V value){
        LRUCache cache = getCacheWithLowerPriority();
        deleteExtraValue();
        boolean result = !put(key, value);
        if(!result) {
            caches.add(cache);
            return false;
        }
        return true;
    }

    @Override
    public void deleteExtraValue() {
        caches.remove(getCacheWithLowerPriority());
    }

    public LRUCache getCacheWithLowerPriority(){
        return caches.last();
    }

    protected LRUCache findByKey(Long key) {
        return caches.stream().filter(lruCache -> lruCache.getKey().equals(key)).findFirst().get();
    }

    protected boolean validateSize(V value){
        Long valueSize = instrumentation.getObjectSize(value);
        if(instrumentation.getObjectSize(value) > capacity){
            throw new ProhibitedActionException("object size > firstLVL capacity");
        }
        return (caches.stream().mapToLong( cachesValueSize -> instrumentation.getObjectSize(cachesValueSize)).sum() + valueSize <= capacity);
    }

    protected boolean validateValue(V value){
        return caches.stream().anyMatch(cache -> cache.getValue().equals(value));
    }

    protected Instrumentation getInstrumentation(){
        return instrumentation;
    }

}
