package сaching.service;

import сaching.data.Cache;
import сaching.data.LFUCache;
import сaching.data.LRUCache;
import сaching.exception.ProhibitedActionException;
import сaching.file.FileWork;

import java.io.*;
import java.time.LocalDateTime;
import java.util.*;

public class LRUCacheServiceFileSys<V> extends LRUCacheService<V> implements CacheServiceFileSys<V> {
    private final FileWork fileWork;
    private final String cacheDirectory;
    private final String pathCacheFile;


    public LRUCacheServiceFileSys(Long capacity,
                                  String cacheDirectory,
                                  String pathCacheFile,
                                  FileWork fileWork) {
        super(capacity);
        this.pathCacheFile = pathCacheFile;
        this.fileWork = fileWork;
        this.cacheDirectory = cacheDirectory;
    }

    @Override
    public boolean put(Long key, V value) {
        TreeSet<LRUCache> caches = deserializationCacheFile();
        if (!validateValue(caches, value) && !validateSize(value, caches)) {
            return  false;
        }
       putAndFormNewIfNeed(key, value, caches);
       return true;
    }

    protected void putAndFormNewIfNeed(Long key, V value, TreeSet<LRUCache> caches) {
        LRUCache cache;
        if (!validateValue(caches, value) && validateKey(key, caches)) {
            cache = new LRUCache(key, value, LocalDateTime.now());
            caches.add(cache);
        } else {
            cache = findByKey(key);
            cache.setValue(value);
            cache.setPriority(LocalDateTime.now());
        }
        fileWork.serializationObj(cache, getCachePath(cache));
        serializationCacheFile(caches);
    }

    private boolean validateKey(Long key, TreeSet<LRUCache> caches){
        if(caches.stream().anyMatch(cache -> cache.getKey().equals(key))){
            throw new ProhibitedActionException("key "+key+" already exist");
        }
        return true;
    }

    public void serializationCacheFile(Set<LRUCache> cache) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(pathCacheFile))) {
            oos.writeObject(cache);
        } catch (IOException ex) {
            throw new ProhibitedActionException("Error write cache to file", ex);
        }
    }

    public TreeSet<LRUCache> deserializationCacheFile() {
        TreeSet<LRUCache> result;
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(pathCacheFile))) {
            result = (TreeSet<LRUCache>) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            throw new ProhibitedActionException("Error read cache to file", ex);
        }
        result.stream().sorted(Comparator.comparing(LRUCache::getPriority));
        return result;
    }

    private String getCachePath (Cache cache){
        return cacheDirectory+"/"+cache.getKey()+cache.getPriority();
    }

    public void deleteExtraValue() {
        LRUCache cache = deserializationCacheFile().last();
        fileWork.deleteFile(cache, getCachePath(cache));
        getCaches().remove(getCaches().last());
    }

    @Override
    public boolean deleteExtraValueAndPut(Long keyCache, V cache) {
        deleteExtraValue();
        return put(keyCache, cache);
    }

    protected boolean validateValue(TreeSet<LRUCache> caches, V value){
        return caches.stream().anyMatch(cache -> cache.getValue().equals(value));
    }

    protected boolean validateSize(V value, TreeSet<LRUCache> caches){
        Long valueSize = getInstrumentation().getObjectSize(value);
         var instrumentation = getInstrumentation();
        if(instrumentation.getObjectSize(value) > getCapacity()){
            throw new ProhibitedActionException("object size > secondLVL capacity");
        }
        return (caches.stream().mapToLong(instrumentation::getObjectSize).sum() + valueSize <= getCapacity());
    }
}
