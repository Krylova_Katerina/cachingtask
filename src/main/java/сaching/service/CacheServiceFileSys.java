package сaching.service;


public interface CacheServiceFileSys <V> {
    boolean put(Long key, V value);

    boolean deleteExtraValueAndPut(Long keyCache, V cache);
}
