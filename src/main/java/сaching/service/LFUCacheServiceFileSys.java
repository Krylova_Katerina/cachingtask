package сaching.service;

import сaching.data.Cache;
import сaching.data.LFUCache;
import сaching.exception.ProhibitedActionException;
import сaching.file.FileWork;

import java.io.*;
import java.util.*;

public class LFUCacheServiceFileSys<V> extends LFUCacheService<V> implements CacheServiceFileSys<V> {
    private final FileWork fileWork;
    private final String cacheDirectory;
    private final String pathCacheFile;

    public LFUCacheServiceFileSys(Long capacity,
                                  String cacheDirectory,
                                  String pathCacheFile,
                                  FileWork fileWork) {
        super(capacity);
        this.fileWork = fileWork;
        this.cacheDirectory = cacheDirectory;
        this.pathCacheFile = pathCacheFile;
    }


    @Override
    public boolean put(Long key, V value) {
        TreeSet<LFUCache> caches = deserializationCacheFile();
        if (!validateValue(caches, value) && !validateSize(value, caches)) {
            return false;
        }
        putAndFormNewIfNeed(key, value, caches);
        return true;
    }


    protected void putAndFormNewIfNeed(Long key, V value, TreeSet<LFUCache> caches) {
        LFUCache cache;
        if (!validateValue(caches, value) && validateKey(key, caches)) {
            cache=new LFUCache(key, value,1L);
            caches.add(cache);
        } else {
            cache = findByKey(key);
            cache.setValue(value);
            cache.setPriority(cache.getPriority()+1);
        }
        fileWork.serializationObj(cache, getCachePath(cache));
        serializationCacheFile(caches);
    }

    private boolean validateKey(Long key, TreeSet<LFUCache> caches){
        if(caches.stream().anyMatch(cache -> cache.getKey().equals(key))){
            throw new ProhibitedActionException("key "+key+" already exist");
        }
        return true;
    }

    public void serializationCacheFile(Set<LFUCache> cache) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(pathCacheFile))) {
            oos.writeObject(cache);
        } catch (IOException ex) {
            throw new ProhibitedActionException("Error write cache to file", ex);
        }
    }

    public TreeSet<LFUCache> deserializationCacheFile() {
        TreeSet<LFUCache> result;
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(pathCacheFile))) {
            result = (TreeSet<LFUCache>) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            throw new ProhibitedActionException("Error read cache to file", ex);
        }
        result.stream().sorted(Comparator.comparing(LFUCache::getPriority));
        return result;
    }

    private String getCachePath (Cache cache){
        return cacheDirectory+"/"+cache.getKey()+cache.getPriority();
    }

    public void deleteExtraValue() {
        LFUCache cache = deserializationCacheFile().last();
        fileWork.deleteFile(cache, getCachePath(cache));
        getCaches().remove(getCaches().last());
    }

    @Override
    public boolean deleteExtraValueAndPut(Long keyCache, V cache) {
        deleteExtraValue();
        return put(keyCache, cache);
    }

    protected boolean validateValue(TreeSet<LFUCache> caches, V value){
        return caches.stream().anyMatch(cache -> cache.getValue().equals(value));
    }

    protected boolean validateSize(V value, TreeSet<LFUCache> caches){
        Long valueSize = getInstrumentation().getObjectSize(value);
        var instrumentation = getInstrumentation();
        if(instrumentation.getObjectSize(value) > getCapacity()){
            throw new ProhibitedActionException("object size > secondLVL capacity");
        }
        return (caches.stream().mapToLong(instrumentation::getObjectSize).sum() + valueSize <= getCapacity());
    }

}
