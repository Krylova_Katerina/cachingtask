package сaching.service;

import lombok.Getter;
import lombok.Setter;
import сaching.data.LFUCache;
import сaching.exception.ProhibitedActionException;

import java.lang.instrument.Instrumentation;
import java.util.*;

@Getter
@Setter
public class LFUCacheService<V> implements CacheService<V> {
    private TreeSet<LFUCache> caches = new TreeSet<>(Comparator.comparing(LFUCache::getPriority));
    private Long capacity;

    private static Instrumentation instrumentation;

    public LFUCacheService(Long capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean put(Long key, V value) {
        if (!validateValue(value) && !validateSize(value)) {
          return false;
        }
        putAndFormNewIfNeed(key, value);
        return true;
    }

    protected void putAndFormNewIfNeed(Long key, V value) {
        if (!validateValue(value) && validateKey(key)) {
            caches.add(new LFUCache(key, value, 1L));
        } else {
            LFUCache cache = findByKey(key);
            cache.setPriority(cache.getPriority()+1);
        }
    }

    private boolean validateKey(Long key){
       if(caches.stream().anyMatch(cache -> cache.getKey().equals(key))){
           throw new ProhibitedActionException("key "+key+" already exist");
       }
       return true;
    }

    @Override
    public boolean validateValueSizeAndPutIfCan(Long key, V value){
        LFUCache cache = getCacheWithLowerPriority();
        deleteExtraValue();
        boolean result = !put(key, value);
        if(!result) {
            caches.add(cache);
            return false;
        }
        return true;
    }

    @Override
    public void deleteExtraValue() {
        caches.remove(getCacheWithLowerPriority());
    }


    public LFUCache getCacheWithLowerPriority(){
        return caches.last();
    }

    protected LFUCache findByKey(Long key) {
        return caches.stream().filter(lfuCache -> lfuCache.getKey().equals(key)).findFirst().get();
    }

    protected boolean validateSize(V value){
        Long valueSize = instrumentation.getObjectSize(value);
        if(instrumentation.getObjectSize(value) > capacity){
            throw new ProhibitedActionException("object size > firstLVL capacity");
        }
        return (caches.stream().mapToLong( cachesValueSize -> instrumentation.getObjectSize(cachesValueSize)).sum() + valueSize <= capacity);
    }

    protected boolean validateValue(V value){
        return caches.stream().anyMatch(cache -> cache.getValue().equals(value));
    }

    protected Instrumentation getInstrumentation(){
        return instrumentation;
    }


}
