package сaching.data;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@EqualsAndHashCode
@ToString
public class LFUCache<V>  implements Cache<Long, Long> {
    private Long key;
    private V value;
    private Long priority;

    public LFUCache(Long key, V value, Long priority) {
        this.key = key;
        this.value = value;
        this.priority = priority;
    }

    public V getValue() {
        return value;
    }
    public void setValue(V value) {
        this.value = value;
    }
}