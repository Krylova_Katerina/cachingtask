package сaching.data;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class LRUCache<V> implements Cache<Long, LocalDateTime>{
private Long key;
private V value;
private LocalDateTime priority;

    public V getValue() {
        return value;
    }
    public void setValue(V value) {
        this.value = value;
    }

    public LRUCache(Long key, V value, LocalDateTime priority) {
        this.key = key;
        this.value = value;
        this.priority = priority;
    }
}
