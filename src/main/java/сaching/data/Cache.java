package сaching.data;

public interface Cache<K, P> {
    K getKey();
    P getPriority();
}
