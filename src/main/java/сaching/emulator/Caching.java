package сaching.emulator;

import lombok.Getter;
import lombok.Setter;
import сaching.file.FileWork;
import сaching.service.CacheService;
import сaching.service.CacheServiceFileSys;


@Getter
@Setter
public class Caching<V> {
    private final CacheService firstLVL;
    private final CacheServiceFileSys secondLVL;
    private final FileWork fileWork;

    public Caching(CacheService firstLVL, CacheServiceFileSys secondLVL, FileWork fileWork) {
        this.firstLVL = firstLVL;
        this.secondLVL = secondLVL;
        this.fileWork = fileWork;
    }

    public void caching(V cache, Long keyCache) {
        var flag = false;
        while (!flag) {
            if (!firstLVL.put(keyCache, cache)) {
                if (!secondLVL.put(keyCache, cache)) {
                    if (!firstLVL.validateValueSizeAndPutIfCan(keyCache, cache)) {
                        flag = secondLVL.deleteExtraValueAndPut(keyCache, cache);
                    }
                }
            }
        }
    }
}
