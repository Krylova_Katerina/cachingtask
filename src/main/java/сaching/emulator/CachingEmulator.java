package сaching.emulator;

import сaching.data.TypeCache;
import сaching.file.FileWork;
import сaching.service.*;

import java.util.List;

public class CachingEmulator {
    private final FileWork fileWork;
    private Long capacityFirstLVL;
    private Long capacitySecondLVL;
    private final String cacheDirectory;
    private final String pathCacheFile;
    private TypeCache type;

    private static Long key = 0L;

    public CachingEmulator(FileWork fileWork, Long capacityFirstLVL,
                           Long capacitySecondLVL, String cacheDirectory,
                           String pathCacheFile, TypeCache type) {
        this.fileWork = fileWork;
        this.capacityFirstLVL = capacityFirstLVL;
        this.capacitySecondLVL = capacitySecondLVL;
        this.cacheDirectory = cacheDirectory;
        this.pathCacheFile = pathCacheFile;
        this.type = type;
    }

    public void emulateCaching(){
        List<String> forCaching = fileWork.readStringsFromFile();
        Caching caching = new Caching(createCacheServiceByType(capacityFirstLVL),
                createCacheServiceFileSysByType(capacitySecondLVL), fileWork);
        forCaching.forEach(cache -> caching.caching(cache, key++));

    }

    private CacheService createCacheServiceByType(Long capacity){
       return type.equals(TypeCache.LFU) ?
               new LFUCacheService(capacity) :
               new LRUCacheService(capacity);
    }

    private CacheServiceFileSys createCacheServiceFileSysByType(Long capacity){
        return type.equals(TypeCache.LFU) ?
                new LFUCacheServiceFileSys(capacity, cacheDirectory, pathCacheFile, fileWork) :
                new LRUCacheServiceFileSys(capacity, cacheDirectory, pathCacheFile, fileWork);
    }
}
